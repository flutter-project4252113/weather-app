import 'package:flutter/material.dart';
import 'package:clima/services/location.dart';
import 'package:clima/screens/location_screen.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:clima/types/weatherInfo.dart';
import 'package:clima/services/weather.dart';

class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  Location location = Location(latitude: 0.0, longitude: 0.0);

  setLocation() async {
    await location.getLocation();
    print(
        "latitude: ${location.latitude} and longitude: ${location.longitude}");
    getData();
    return;
  }

  getData() async {
    WeatherModel weatherModel = WeatherModel();

    WeatherInfo weatherData = await weatherModel.getLocationWeather(location);

    if (weatherData != null) {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return LocationScreen(locationWeather: weatherData);
      }));
    } else {
      print('Failed to get data.');
    }
  }

  @override
  void initState() {
    setLocation();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SpinKitDoubleBounce(
          color: Colors.white,
          size: 100.0,
        ),
      ),
    );
  }
}
