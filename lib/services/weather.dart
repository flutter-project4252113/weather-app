import 'package:clima/services/networking.dart';
import 'package:clima/services/location.dart';
import 'package:clima/types/weatherInfo.dart';

class WeatherModel {
  static const apiKey = "4a5c04286a35ca1f8123621a28cdbb5e";
  static const openWeatherMapURL =
      'https://api.openweathermap.org/data/2.5/weather';

  String getWeatherIcon(int condition) {
    if (condition < 300) {
      return '🌩';
    } else if (condition < 400) {
      return '🌧';
    } else if (condition < 600) {
      return '☔️';
    } else if (condition < 700) {
      return '☃️';
    } else if (condition < 800) {
      return '🌫';
    } else if (condition == 800) {
      return '☀️';
    } else if (condition <= 804) {
      return '☁️';
    } else {
      return '🤷‍';
    }
  }

  String getMessage(int temp) {
    if (temp > 77) {
      return 'It\'s 🍦 time';
    } else if (temp > 68) {
      return 'Time for shorts and 👕';
    } else if (temp < 50) {
      return 'You\'ll need 🧣 and 🧤';
    } else {
      return 'Bring a 🧥 just in case';
    }
  }

  Future<WeatherInfo> getLocationWeather(Location location) async {
    String url =
        '${openWeatherMapURL}?lat=${location.latitude}&lon=${location.longitude}&appid=$apiKey&units=imperial';

    NetworkHelper networkHelper = NetworkHelper(url);
    var weatherData = await networkHelper.getData();

    if (weatherData != null) {
      double temp = weatherData['main']['temp'];
      int code = weatherData['weather'][0]['id'];
      String cityName = weatherData['name'];
      WeatherInfo weatherInfo =
          WeatherInfo(cityName: cityName, temperature: temp, code: code);
      return weatherInfo;
    } else {
      print('Failed to get data.');
      return WeatherInfo(cityName: '', temperature: 0.0, code: 0);
    }
  }

  Future<WeatherInfo> getCityWeather(String cityName) async {
    String url =
        '${openWeatherMapURL}?q=$cityName&appid=$apiKey&units=imperial';

    NetworkHelper networkHelper = NetworkHelper(url);
    var weatherData = await networkHelper.getData();

    if (weatherData != null) {
      double temp = weatherData['main']['temp'];
      int code = weatherData['weather'][0]['id'];
      String cityName = weatherData['name'];
      WeatherInfo weatherInfo =
          WeatherInfo(cityName: cityName, temperature: temp, code: code);
      return weatherInfo;
    } else {
      print('Failed to get data.');
      return WeatherInfo(cityName: '', temperature: 0.0, code: 0);
    }
  }
}
