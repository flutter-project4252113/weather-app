class WeatherInfo {
  final String cityName;
  final double temperature;
  final int code;

  WeatherInfo(
      {required this.cityName, required this.temperature, required this.code});

  factory WeatherInfo.fromJson(Map<String, dynamic> json) {
    return WeatherInfo(
      cityName: json['name'],
      temperature: json['main']['temp'],
      code: json['weather'][0]['id'],
    );
  }
}
